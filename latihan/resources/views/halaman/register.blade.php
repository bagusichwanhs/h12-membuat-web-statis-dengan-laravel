<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            @csrf
            <label>First Name:</label><br><br>
            <input type="text" name="nama1" required><br><br>
            <label>Last Name:</label><br><br>
            <input type="text" name="nama2" required><br><br>
            <label>Gender:</label><br><br>
            <input type="radio" value="MALE" name="Gender" required>MALE <br>
            <input type="radio" value="FEMALE" name="Gender" required>FEMALE <br>
            <input type="radio" value="OTHER" name="Gender" required>OTHER <br><br>
            <label>Nationality:</label><br><br>
            <select name="Nationality" required>
                <option value="Indonesia">Indonesia</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Singapore">Singapore</option>
                <option value="Australia">Australia</option>
            </select><br><br>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" >Bahasa Indonesia<br>
            <input type="checkbox" >English<br>
            <input type="checkbox" >Other<br><br>
            <label>Bio:</label><br><br>
            <textarea name="bio" cols="30" rows="30"></textarea><br><br>
            <input type="submit" value="welcome">
        </form>
</body>
</html>